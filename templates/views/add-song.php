<div class = 'introText'>
    <h1>Add a new song</h1>
<?php if($locals['showSuccessMessage']) { ?>
<p>successfully added!</p>
<?php } else if ($locals['songUnsuccessfull']) {?>
<p>All fields in the song table must be filled in.</p>
<p>Missing: <?=$locals['var'] ?></p>
<?php } ?>

</div>


<?=$locals['render']?>

<div class = 'introText'>
                <form action='' method='post'>
                    <label for= 'song_name'>Song Name: </label>
                    <input type='text' id='song_name' name='song_name' placeholder= '    eg. blah'>
                    <br>
                    <label for= 'artist'>Artist: </label>
                    <input type='text' id='artist' name='artist' placeholder= '    eg. some guy' pattern="^((\w)+?)$">
                    <br>
                    <label for= 'run_time'>Run time: </label>
                    <input type='time' id='run_time' name='run_time' placeholder= '    eg. 5:00'pattern="^\d{1-2}:\d{2}$">
                    <br>

                    <label for= 'album_id'>AID: </label>
                    <select name = 'album_id' id='album_id'>
                        <?php foreach ($locals['displayPage'] as $album) { ?>
                            <option value='<?= $album['game_series'] ?>'><?= $album['game_series'] ?></option>
                        <?php } ?>
                   
                    </select>
                            <br>
                            <br>
                    <input type='submit' value='send this!'>
                </form>
</div>
<div class = 'introText'>
                <h2><?= $locals['delete']?></h2>
                <p><?= $locals['side-right-text'] ?></p>
</div>
    


    
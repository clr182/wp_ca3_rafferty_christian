<?php

//create a database PDO here.
$config = include('config.php');


define('APP_BASE_URL', $config['app_base_url']);

  // Include the Rapid library
  require_once('lib/Rapid.php');



  

  // Create a new Router instance
  $app = new \Rapid\Router();
  // Process the request
  $app->GET('/',               'Home');
  $app->GET('/home',           'Home');
  $app->GET('/contact',        'Contact');
  $app->GET('/view-songs',     'ViewSongs');
  $app->POST('/view-songs',    'ViewSongsProcess');
  $app->GET('/add-song',       'AddSong');
  $app->POST('/add-song',      'AddToTablesProcess');
  $app->GET('/add-game',       'AddGame');
  $app->POST('/add-game',      'AddToTablesProcess');
  $app->GET('/error',          'error');
  $app->dispatch();
?>
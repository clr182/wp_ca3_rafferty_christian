# VGOSTDB

Video Game Original SoundTrack Database

## What is the aim of this site?

The aim of this site is to display a user friendly database of video game original soundtracks. I made a one to many database,
One Album has many songs. This Assignment was made to demonstrate use of the Rapid MVC web framework provided by Leccturer
Shane Gavin, of **[Dundalk Institute of Technology](https://www.dkit.ie)**

## Criteria of CA3 still to be done
 - Update for both table entities
 - Exception handling


## References

 - The CSS used for the navigation bar found on my site was found on w3schools.com
  **[w3schools.com](https://www.w3schools.com/css/tryit.asp?filename=trycss_navbar_horizontal_black)**

---

 - The font used in this site is Computer Pixel-7
  **[daFont.com](https://www.dafont.com/computer-pixel-7.font)**

---

 - reference
  **[site-name](link address)**

---

 - I'd like to reference Shane Gavin for providing the Rapid MVC framework used in this project and
 - Cameron Scholes for pointing out a bug in my index page cleared up my confusion using the require_once
 - function. No code was taken from him, but he did provide help.
 - I'd like to reference James Farrell for fixing my drop down album display box.

---

## Author

**Name:** Christian Rafferty,

**D-number:** D00188224,

**Class:** GD2A,

**Created:** Wednesday 20/19/2019.

### if you wish to contact me, you can do so at _chrustuanr15@gmail.com_

<?php
    $config = include ('config.php');

    $db = new PDO(
        'mysql:host=' .
        $config['db_host'] .
        ';dbname=' . 
        $config['db_name'] . 
        ';charset=latin1',
        $config['db_username'],
        $config['db_password']
    );
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
    return $db;
    ?>

<?php 
//examples of classes and OOP.

class Song {

    
    //rows is to represent a row in a DB
    public function __construct($row) {
        $this->song_id      = $row['song_id'];
        $this->song_name    = $row['song_name'];
        $this->artist       = $row['artist'];
        $this->run_time     = $row['run_time'];
        $this->album_id     = $album_id['album_id'];

    }

    public function addNewSong($album_id,$song_name, $artist, $run_time){
        $db = include_once('database.php');

        try{
            $query = "INSERT INTO song(album_id, song_name, artist, run_time) 
            VALUES(:album_id, :song_name, :artist, :run_time)";
            $statement = $db->prepare($query);
            $statement->bindParam(":album_id",   $album_id);
            $statement->bindParam(":song_name",  $song_name,   PDO::PARAM_STR);
            $statement->bindParam(":artist",     $artist,      PDO::PARAM_STR);
            $statement->bindParam(":run_time",   $run_time,    PDO::PARAM_STR);
            $statement->execute();
        }
        catch (PDOException $e){
            echo ($e->getMessage());
        }
    }

    public function delete($song_name){
        $db = require('database.php');

        try{
            $query = "DELETE FROM song WHERE song_name = :song_name";
            $statement = $db->prepare($query);
            $statement->bindParam(":song_name", $song_name, PDO::PARAM_INT);
            $statement->execute();
        }
        catch(PDOException $e){
            echo ($e->getMessage());
        }

    }

    public function listSongsByAllDetails(){
        $db = require('database.php');

        try{
            $query = "SELECT song_name, artist, run_time FROM song";
            $statement = $db->prepare($query);
            $statement->execute();
            
            // $result = null;
            $result = $statement->fetchAll(PDO::FETCH_OBJ);

            foreach ($result as $row)
            {
                $result .=
                 "<tr><td>" .$row->song_name . "</td>" .
                 "<td>" .    $row->artist . "</td>" .
                 "<td>" .    $row->run_time . "</td></tr>";
            }
            $statement->closeCursor();
            return $result;
        }
    
        catch (PDOException $e){
            echo ($e->getMessage());
        }
    }

    
}
?>






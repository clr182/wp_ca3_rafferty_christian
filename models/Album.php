<?php 
//examples of classes and OOP.
class Album {
    
    public function __construct($row) {
        $this->album_id         = $row['album_id'];
        $this->game_series      = $row['game_series'];
        $this->release_year     = $release_year['release_year'];
        $this->date_added_to_db = $date_added_to_db['date_added_to_db'];
    }
    
    public function addNewGame($game_series, $release_year){
        $db = include_once('database.php');

        try{
            $query = "INSERT INTO album(game_series, release_year) 
            VALUES(:game_series, :release_year)";
            $statement = $db->prepare($query);
            $statement->bindParam(":game_series"     , $game_series,  PDO::PARAM_STR);
            $statement->bindParam(":release_year"    , $release_year, PDO::PARAM_INT);
            $statement->execute();
        }
        catch (PDOException $e){
            echo ($e->getMessage());
        }
    }

    public function listAlbumsByAllDetails(){
        $db = include_once('database.php');

        try{
            $query = "SELECT * FROM album";
            $statement = $db->prepare($query);
            $statement->execute();
            
            $result = null;
            $result = $statement->fetchAll(PDO::FETCH_OBJ);
            foreach($result as $row)
            {
                $result .=
                "<tr><td>" .
                $row->game_series . "</td>" .
                "<td>" .  $row->release_year . "</td>" .
                "<td>" . $row->date_added_to_db . "</td></tr>"
                 ;
            }
            $statement->closeCursor();
            return $result;
            
        }
        catch (PDOException $e){
            echo ($e->getMessage());
        }
    }

    public function delete($game_series){
        $db = require('database.php');

        try{
            $query = "DELETE FROM album WHERE game_series = :game_series";
            $statement = $db->prepare($query);
            $statement->bindParam(":game_series", $game_series, PDO::PARAM_STR);
            $statement->execute();
        }
        catch(PDOException $e){
            echo ($e->getMessage());
        }

    }

    public function displayAlbumNamesForOptionSelect(){
        $db = include_once('database.php');
        
        try{
            $query = "SELECT game_series FROM album ORDER BY game_series ASC";
            $statement = $db->query($query);
            $statement->execute();
            $results = $statement->fetchAll();

            $statement->closeCursor();
            return $results;
        }
        catch (PDOException $e){
            echo ($e->getMessage());
        }
    }


    public function displayByDate(){
        $db = include_once('database.php');

        try{
            $query = "SELECT game_series From album ORDER BY date_added_to_db DESC";
            $statement = $db->prepare($query);
            $statement->execute();
            $results = null;
            $select = $statement->fetchAll(PDO::FETCH_OBJ);
            foreach($select as $row){
                $results .= "<div class='introText'>" . "game name: " . $row->game_series . "<br>"
                 . "</div>";
            }
        $statement->closeCursor();
        return $results;
        }
        catch(PDOException $e){
            echo ($e->getMessage());
        }
    }



}
?>
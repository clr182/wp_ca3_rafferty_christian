<?php 
//examples of classes and OOP.

class createDB {

    public function createDatabase($dbName){
        $db = include_once('database');

        try{
            $query = "CREATE DATABASE IF NOT EXISTS $dbName";
            $statement = $db->prepare($query);
            $statement->execute();
        }
        catch (PDOException $e){
            echo ($e->getMessage());
        }
    }
        public function createTable(){
        $db = include_once('database');

        try{
            $query = "DROP TABLE IF EXISTS album"
            $statement = $db->prepare($query);
            $statement->execute();
            $query = "DROP TABLE IF EXISTS song"
            $statement = $db->prepare($query);
            $statement->execute();
        }
        catch (PDOException $e){
            echo ($e->getMessage());
        }

        try{
        $query = "CREATE TABLE `album` (
            `album_id` bigint(20) UNSIGNED NOT NULL,
            `game_series` varchar(255) NOT NULL,
            `release_year` year(4) NOT NULL,
            `date_added_to_db` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP)";

        $statement = $db->prepare($query);
        $statement->execute();


        $query = "CREATE TABLE `song` (
            `song_id` bigint(20) UNSIGNED NOT NULL,
            `song_name` varchar(255) NOT NULL,
            `artist` varchar(255) NOT NULL,
            `run_time` time NOT NULL,
            `album_id` bigint(20) UNSIGNED NOT NULL)";
          
        $statement = $db->prepare($query);
        $statement->execute();
        }

        catch (PDOException $e){
            echo ($e->getMessage());
        }
    }

    public function setPrimaryKeys(){
        $db = include_once('database');

        try{
        $query = "ALTER TABLE `song`
        ADD PRIMARY KEY (`song_id`),
        ADD KEY `album_id` (`album_id`)";
        $statement = $db->prepare($query);
        $statement->execute();
    
    
        $query = "ALTER TABLE `album`
        ADD PRIMARY KEY (`album_id`)";
        $statement = $db->prepare($query);
        $statement->execute();
        
        $query = "ALTER TABLE `album`
        MODIFY `album_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2)";
        $statement = $db->prepare($query);
        $statement->execute();

        $query = "ALTER TABLE `song`
        MODIFY `song_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=852)";
        $statement = $db->prepare($query);
        $statement->execute();

        $query = "ALTER TABLE `song`
        ADD CONSTRAINT `song_ibfk_1` FOREIGN KEY (`album_id`) REFERENCES `album` (`album_id`);
      COMMIT;";
        $statement = $db->prepare($query);
        $statement->execute();
    }
    catch (PDOException $e){
        echo ($e->getMessage());
    }
}

    }
<?php
    $con = mysqli_connect(
    $config['db_host'],
    $config['db_username'],
    $config['db_password'], 
    $config['db_name'])
    or die($config['db_error_message']);

    if($con == false){
        echo $config['db_error_message'];
    }
    else if($con == true){
        echo 'connection made';
     return $con;
    }
?>
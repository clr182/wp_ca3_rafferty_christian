<?php return function($req, $res){
require('models/Song.php');
require('models/Album.php');

$song_name         = $req->body('song_name');
$game_series       = $req->body('game_series');

Song::delete($song_name);
Album::delete($game_series);
$res->redirect('/view-songs?success=1');
}?>
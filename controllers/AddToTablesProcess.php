<?php return function($req, $res){
require('models/Song.php');
require('models/Album.php');





    $thisAddress = $_SERVER['REDIRECT_URL'];
    if($thisAddress == '/wp_ca3_rafferty_christian/add-game'){
        
        $game_series      = $req->body('game_series');
        $release_year     = $req->body('release_year');


        $game_series        = ltrim(rtrim(filter_input(INPUT_POST, "game_series", FILTER_SANITIZE_STRING)));
        $release_year       = ltrim(rtrim(filter_input(INPUT_POST, "release_year", FILTER_SANITIZE_NUMBER_INT)));
        
        if(empty($_POST['game_series']) || empty($_POST['release_year'])){
            $res->redirect('/add-game?unSuccessful=1');
        }
        else{
            Album::addNewGame($game_series, $release_year);
            $res->redirect('/view-songs?success=1');
        }
        
    }
    else if($thisAddress == '/wp_ca3_rafferty_christian/add-song'){
        $song_name        = $req->body('song_name');
        $artist           = $req->body('artist');
        $run_time         = $req->body('run_time');

        $timePattern = "~\d{2}(^:)\d{2}~";
        
        if(preg_match($timePattern)){
            $timePattern .= $timePattern . ":00";
        }

        $album_id         = $req->body('album_id');

        $song_name   = ltrim(rtrim(filter_input(INPUT_POST, "song_name", FILTER_SANITIZE_STRING)));
        $artist      = ltrim(rtrim(filter_input(INPUT_POST, "artist", FILTER_SANITIZE_STRING)));
        $run_time    = ltrim(rtrim(filter_input(INPUT_POST, "artist", FILTER_SANITIZE_SPECIAL_CHARS)));
        
        if(empty($_POST['song_name']) || empty($_POST['artist'])  || empty($_POST['run_time']) || empty($_POST['album_id'])){
            $res->redirect('/add-song?unSuccessful=1');
        }
        else{
            Song::addNewSong($album_id, $song_name, $artist, $run_time);
            $res->redirect('/view-songs?success=1');
        }
        
    }
    
} ?>
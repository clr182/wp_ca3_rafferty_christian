<?php return function($req, $res){

require('models/Song.php');
require('models/Album.php');

$res->render('main', 'view-songs', [
    'PageTitle'       => 'View Songs',
    'side-left'       => 'Complete Song list',

    'side-right'      => 'Album list',
    'album-list'      => Album::listAlbumsByAllDetails(),
    'song-list'       => Song::listSongsByAllDetails(),
]);
}
?>
<?php 
include_once('models/Album.php');
return function($req, $res){
    $res->render('main', 'home', [
        'PageTitle' => 'Home',
        'side-left' => 'NEW CONTENT',
        'side-left-text' => 'what\'s new?',
        'middle' => 'Like what you see?',
        'middle-text' => '<p>maybe we are missing a soundtrack?</p> why not add to the database! you can do so here ',
        'side-right' => 'right column',
        'side-right-text' => 'right text',
        'order-by-date'  => Album::displayByDate()
    ]);
} ?>


<?php 
include_once('models/Song.php');
include_once('models/Album.php');

return function($req, $res){

$res->render('main', 'add-song', [
    //'showSuccessMessage'   => $req-> query('success') === '1',
    'songUnsuccessfull'    => $req-> query('unSuccessful') === '1',
    'albumUnsuccessfull'   => $req-> query('unSuccessful') === '1',
    'PageTitle'            => 'For Administration only, add OST\'s below',
    'displayPage'          => Album::displayAlbumNamesForOptionSelect()
]);
} ?>